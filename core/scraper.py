import config.config as cfg
import scalpl
import requests
from bs4 import BeautifulSoup

headers = scalpl.Cut(cfg.AVAILABLE_HEADERS, sep='->')
search_params_cfg = scalpl.Cut(cfg.BEAUTIFULSOUP_SEARCH_PARAMETERS, sep='->')


def scrape_wiki_page(search_term, desired_value, stop_condition):
    print(f"Running stop condition {stop_condition}\nSearch term: {search_term}\nLooking for: {desired_value}")
    if stop_condition > 5:
        return False, []

    search_url = f"{cfg.WIKIPEDIA_BASE_URL}/{search_term}"
    searchable_links = []

    failed = False
    try:
        r = requests.get(search_url, headers=headers["BASIC"])
        soup = BeautifulSoup(r.text, 'html.parser')
        a_tags = soup.find_all('a', href=search_params_cfg["find_all->href"],
                               limit=search_params_cfg["find_all->limit"])

        spans = soup.find("span", {"class": "mw-page-title-main"})
        wiki_value = spans.text.strip().lower()
        if wiki_value == desired_value.lower():
            return True, search_url

        for a_tag in a_tags:
            link = a_tag["href"]
            if link and link.startswith("/"):
                searchable_links.append(f"{cfg.WIKIPEDIA_BASE_URL}{link}")
    except Exception as ex:
        print(f"{ex}")
        failed = True
    finally:
        if failed:
            return False, searchable_links

    for search in searchable_links:
        x = search.rsplit('/', 1)
        y = x[-1]
        status, searchable_links = scrape_wiki_page(search.rsplit('/', 1)[-1], desired_value, stop_condition + 1)
        if status:
            return True, searchable_links
