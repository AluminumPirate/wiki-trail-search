# import config.config as cfg
import core.scraper as wsearch


def main(argv, search_for):
    status, searchable_links = wsearch.scrape_wiki_page(argv, search_for, 0)

    if not status:
        print(f"had exceptions")
    if not searchable_links or len(searchable_links) <= 0:
        print(f"Search came out empty")


if __name__ == "__main__":
    main("wolf", "cat")
