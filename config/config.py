import os

AVAILABLE_HEADERS = {
    "BASIC": {"Accept-Language": "en-US,en;q=0.9",
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                             "Chrome/98.0.4758.102 Safari/537.36"}
}

WIKIPEDIA_BASE_URL = "https://en.wikipedia.org/wiki"

BEAUTIFULSOUP_SEARCH_PARAMETERS = {
    "find_all": {
        "href": True,
        "limit": 50,
        "recursive": False
    }
}
